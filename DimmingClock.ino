/// Dimming Clock 1.0
/// Justin Arruda

#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>
#include <LiquidCrystal.h>
#include <Encoder.h>

#define VERSION "0.2"

/**
 * Pinout
 *
 * This is the hookup for the rotary encoder
 * D02 -> ENC_B
 * D03 -> ENC_A
 * D04 -> ENC_BTN
 *
 * This is the pinout for the LCD connector (see schematic)
 * D07 -> LCD_RS
 * D08 -> LCD_EN
 * D09 -> LCD_BL+  - This must be a PWM-capable pin.
 * D10 -> LCD_D7
 * D11 -> LCD_D6
 * D12 -> LCD_D5
 * D13 -> LCD_D4
 * 
 * This is our input for the dimmer function (we're using an LDR cell)
 * A0  -> DIM_INPUT
 *
 * These are our i2c lines, which we're using to talk to the real-time clock
 * A4  -> DS1307_SDA
 * A5  -> DS1307_SCL
 **/
#define ENC_BTN 4
#define ENC_A 3
#define ENC_B 2

#define LCD_RS 7
#define LCD_EN 8
#define LCD_BL 9
#define LCD_D7 10
#define LCD_D6 11
#define LCD_D5 12
#define LCD_D4 13

#define DIM_INPUT A0

// Set up our 16x2 display in 4-bit mode
LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);

// Encoder reads the rotary encoder knob for controlling the clock
Encoder encoder(ENC_A, ENC_B);

// Month abbreviations
const char* Months[] = {
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
};

// Days of the week
const char* Days[] = {
    "Sunday   ",
    "Monday   ",
    "Tuesday  ",
    "Wednesday",
    "Thursday ",
    "Friday   ",
    "Saturday "
};


// The current time
tmElements_t gNow;

// The display mode
typedef enum {
    MODE_CLOCK,
    MODE_SET_TIME
} DisplayMode;

// The stage of time editing
typedef enum {
    SET_HOUR,
    SET_MINUTE,
    SET_MONTH,
    SET_DAY,
    SET_YEAR
} SetState;

// The current state of the encoder
typedef struct {
    bool bBtnDown;
    bool bBtnPressed;
    int knobTurned;
} EncoderState;

int           gMode = MODE_CLOCK;
int           gSetState = SET_HOUR;
EncoderState  gEncoderState = {0};
bool          gClockDirty = true;

/**
 * This is where we initialize our clock and everything attached to it.
 */
void setup()
{
    // Tell the time library to retrieve time from the RTC
    setSyncProvider(RTC.get);
    
    // Initialize the LCD display
    lcd.begin(16, 2);
    // Configure the LCD cursor to not blink when it's displayed by default
    lcd.noBlink();
    
    // Here we specify that we want to use an analog pin for input
    pinMode(DIM_INPUT, INPUT);
    // We will dim the backlight with PWM output
    pinMode(LCD_BL, OUTPUT);
    
    // We want to use a digital pin as input and turn on its pull-up resistor
    // When the button is pressed, it will connect it to ground, pulling it LOW
    pinMode(ENC_BTN, INPUT_PULLUP);

    // If the RTC is not running, set the date and time when we compiled this sketch
    if (timeStatus() != timeSet)
    {
        tmElements_t defaultTime = {0};
        defaultTime.Day = 1;
        defaultTime.Month = 1;
        defaultTime.Year = CalendarYrToTm(2000);
        
        setTime(makeTime(defaultTime));
    }
    
    // Now that our pins are configured and everything is initialized,
    // show our startup screen before displaying the clock
    showStartup();
}

/**
 * This is where we place our logic that needs to be executed over and over.
 */
void loop()
{
    updateEncoderState();
    
    switch (gMode)
    {
    case MODE_CLOCK:
        showClock();
        break;
        
    case MODE_SET_TIME:
        showClockSet();
        break;
    }
}

/**
 * This function updates gEncoderState to reflect 

/**
 * This function shows a message on startup for 2 seconds, then clears the screen.
 */
void showStartup()
{
    // Turn backlight to full brightness
    digitalWrite(LCD_BL, HIGH);
    
    // Clear the display and show startup banner
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("DimmerClock v" VERSION);
    
    // Let the banner show for 2 seconds
    delay(2000);
    
    // Clear it out before resuming
    lcd.clear();
}

/**
 * This function shows the current time on the top line and the date on the bottom.
 * The screen does not have to be cleared in between calls to it, but should start cleared.
 */
void showClock()
{
    // Check for a mode change
    if (gEncoderState.bBtnPressed) {
        setClockMode(MODE_SET_TIME);
        return;
    }
        
    // Keep track of the currently displayed minute
    int lastMinute = gNow.Minute;

    // Read in the current time
    time_t time_now = now();
    breakTime(time_now, gNow);
    
    // Check if we need to redisplay the time
    if (gNow.Minute == lastMinute && !gClockDirty) {
        // No need to redisplay, early out
        return;
    }
    
    // Clear the display if the clock is dirty
    if (gClockDirty) {
        lcd.clear();
        gClockDirty = false;
    }
    
    // Print the new time to the display
    printTime(&gNow);
    
    // Set backlight brightness
    float backlight = (1.0f - analogRead(DIM_INPUT)) / 1023.0f;
//    analogWrite(LCD_BL, backlight * 255);
}

void showClockSet() {
    // Check for a mode change
    if (gEncoderState.bBtnPressed) {
        // SET_HOUR -> SET_MINUTE -> SET_PERIOD -> SET_DAY -> SET_MONTH -> SET_YEAR
        ++gSetState;
        gClockDirty = true;
        
        // When completing the set states, return to displaying clock
        if (gSetState > SET_YEAR) {
            // Update clock before returning to display
            time_t time_now = makeTime(gNow);
            breakTime(time_now, gNow);

            setTime(time_now);
            RTC.write(gNow);
            
            setClockMode(MODE_CLOCK);
            return;
        }
    }
    
    if (gEncoderState.knobTurned) {
        modifyField(gSetState, &gNow, gEncoderState.knobTurned);
        gClockDirty = true;
    }
    
    if (!gClockDirty) {
        return;
    }
    
    if (gSetState != SET_YEAR) {
        printTime(&gNow);
    }
    else {
        lcd.clear();
        lcd.setCursor(6, 0);
        lcd.print(tmYearToCalendar(gNow.Year), DEC);
    }
    
    // Place cursor according to field
    switch (gSetState) {
        case SET_HOUR:
            lcd.setCursor(5, 0);
            break;
        case SET_MINUTE:
            lcd.setCursor(8, 0);
            break;
        case SET_MONTH:
            lcd.setCursor(12, 1);
            break;
        case SET_DAY:
            lcd.setCursor(15, 1);
            break;
        case SET_YEAR:
            lcd.setCursor(9, 0);
            break;
    }
    
    gClockDirty = false;
}

void setClockMode(int mode) {
    switch (mode) {
        case MODE_CLOCK:
            lcd.noBlink();
            lcd.clear();
            lcd.setCursor(5, 0);
            lcd.print("Clock");
            delay(1000);
            break;
            
        case MODE_SET_TIME:
            lcd.clear();
            lcd.setCursor(4, 0);
            lcd.print("Set Time");
            delay(1000);
            lcd.blink();
            
            gSetState = SET_HOUR;
            break;
    }

    gClockDirty = true;    
    gMode = mode;
}

void updateEncoderState() {
    gEncoderState.bBtnPressed = false;
    gEncoderState.knobTurned = 0;
    
    // Check for knob rotation
    long pos = encoder.read();
    
    // This is a quad encoder - 4 "ticks" per detent
    // On each detent, reset the encoder position back to 0
    if (pos != 0 && (pos % 4) == 0) {
        if (pos > 0) {
            gEncoderState.knobTurned = 1;
        }
        else {
            gEncoderState.knobTurned = -1;
        }
        
        encoder.write(0);
    }
    
    // Handle button presses    
    if (digitalRead(ENC_BTN) == LOW) {
        // Debounce button press
        delay(10);
        if (digitalRead(ENC_BTN) == LOW) {
            gEncoderState.bBtnDown = true;
        }
    }
    else {
        if (gEncoderState.bBtnDown) {
            gEncoderState.bBtnPressed = true;
        }
        gEncoderState.bBtnDown = false;
    }
}

void printTime(tmElements_t* time) {
    // We'll use this as a temporary buffer for formatting what we display
    char linebuffer[17] = {0};
    time_t time_now = makeTime(*time);
    
    // Convert 24-hour time into 12-hour
    int hr = hourFormat12(time_now);
    const char* period = isAM(time_now) ? "am" : "pm";
    
    // Time centered on top line 
    sprintf(linebuffer, "%2d:%02d %s", hr, time->Minute, period);
    lcd.setCursor(4, 0);
    lcd.print(linebuffer);
    
    // Full day of week is left aligned on second row
    lcd.setCursor(0, 1);
    lcd.print(Days[time->Wday - 1]);
    
    // Finally, abbreviated month and day digit to the right, second row
    sprintf(linebuffer, "%s %02d", Months[time->Month - 1], time->Day);
    lcd.setCursor(10, 1);
    lcd.print(linebuffer);
}

int constrainValue(int value, int dv, int lo, int hi) {
    value += dv;
    
    if (value > hi) {
        value = lo;
    }
    else if (value < lo) {
        value = hi;
    }
        
    return value;
}

void modifyField(int setMode, tmElements_t* time, int dt) {
    switch (setMode) {
        case SET_HOUR:
            time->Hour = constrainValue(time->Hour, dt, 0, 23);
            break;
        case SET_MINUTE:
            time->Minute = constrainValue(time->Minute, dt, 0, 59);
            break;
        case SET_MONTH:
            time->Month = constrainValue(time->Month, dt, 1, 12);
            break;
        case SET_DAY:
            time->Day = constrainValue(time->Day, dt, 1, 31);
            break;
        case SET_YEAR:
            time->Year += dt;
            break;
    }
}
